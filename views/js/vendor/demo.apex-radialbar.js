var colors = ["#CC7000"],
    dataColors = $("#basic-radialbar").data("colors");
dataColors && (colors = dataColors.split(","));
var options = {
        chart: {
            height: 320,
            type: "radialBar"
        },
        plotOptions: {
            radialBar: {
                hollow: {
                    size: "70%"
                }
            }
        },
        colors: colors,
        series: [70],
        labels: ["silver"]
    },
    chart = new ApexCharts(document.querySelector("#basic-radialbar"), options);
chart.render();
