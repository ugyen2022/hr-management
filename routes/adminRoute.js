const express=require("express");
const admin_route= express();
const session = require("express-session");
const config = require("../config/config");
const path = require("path");
const userController = require("../controllers/userController")
const adminController = require("../controllers/adminController");


admin_route.use(session({secret:config.sessionSecret,  resave: false, // set to false to avoid deprecated warning
saveUninitialized: false // set to false to avoid deprecated warning
}));

const auth = require("../middleware/auth");



const multer = require("multer");

admin_route.use(express.static('public'));

const storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(__dirname, '../public/userImages'));
    },
    filename:function(req,file,cb){
        const name =Date.now()+'-'+file.originalname;
        cb(null, name);
    }
});

const upload = multer({storage:storage});




admin_route.post('/register', upload.single('image'),adminController.registerAdmin);

// admin_route.get('/Manageuser',auth.isLogin,adminController.loadManageuser)

admin_route.get('/adminDashboard',auth.isLogin,userController.restrictTo('admin'),adminController.loadDashboard,)


admin_route.get('/addIndividual',auth.isLogin,userController.restrictTo('admin'),adminController.loadIndividualRegisteration)
admin_route.post('/addIndividual',adminController.IndividualRegisterationEmployee);




admin_route.get('/editEmployee/:id',auth.isLogin,userController.restrictTo('admin'),adminController.loadEditEmployee);
admin_route.post('/editEmployee/:id',adminController.updateEmployee);



admin_route.get('/setting',auth.isLogin,userController.restrictTo('admin'),adminController.loadsetting);
admin_route.get('/employeeProfile',auth.isLogin,userController.restrictTo('admin'),adminController.loademployeeprofile);


admin_route.post('/delete-user',auth.isLogin,userController.restrictTo('admin'),adminController.deleteEmployeeLoad);


admin_route.post('/updateProfile',upload.single('image'),adminController.updateProfile);
admin_route.post('/updatePassword',auth.isLogin,adminController.updatePassword);


admin_route.post('/search-by-gender',auth.isLogin,adminController.postGender);



module.exports=admin_route;
