const express=require("express");
const user_route= express();
const session = require("express-session");
const config = require("../config/config");
const path = require("path");
const auth = require("../middleware/auth");

const userController = require("../controllers/userController");


user_route.use(session({secret:config.sessionSecret,  resave: false, // set to false to avoid deprecated warning
saveUninitialized: false // set to false to avoid deprecated warning
}));



const multer = require("multer");

user_route.use(express.static('public'));

const storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(__dirname, '../public/userImages'));
    },
    filename:function(req,file,cb){
        const name =Date.now()+'-'+file.originalname;
        cb(null, name);
    }
});

const upload = multer({storage:storage});



user_route.get('/register',userController.loadRegister);

// user_route.get('/verify', userController.verifyMail);

user_route.get('/', userController.loginLoad);
user_route.get('/login', userController.loginLoad);

user_route.post('/login', userController.verifyLogin);

user_route.get('/logout',auth.isLogin,userController.logout);



user_route.get('/reset-password',userController.resetPasswordLoad);
user_route.post('/reset-password', userController.resetPassword);



module.exports=user_route;
