const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config({ path: './config.env' })
const app = require('./app')

const DB = process.env.DATABASE.replace(
    'PASSWORD',
    process.env.DATABASE_PASSWORD,
)
const local_db = process.env.DATABASE_LOCAL
mongoose.connect(DB).then((con) => {
    // console.log(con.connections)gti 
    console.log('DB connection succesful')
}).catch(error => console.log(error));

const port = 4000

app.listen(port,()=>{
    console.log(`app is running on port ${port}..`)
})