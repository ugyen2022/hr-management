const express = require("express")
const path = require('path')
const bodyParser = require('body-parser');
const session = require('express-session');
const flash = require('connect-flash');
const app = express()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}))
app.set("view engine", "ejs");

//for admin routes
const userRoute=require('./routes/userRoutes');
const adminRoute=require('./routes/adminRoute');

app.use('/',userRoute);
app.use('/',adminRoute);
app.use(express.static(path.join(__dirname, "views")));
module.exports = app