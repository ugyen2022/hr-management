const mongoose = require("mongoose");

const employeeSchema = new mongoose.Schema({
  name: { type: String },
  employee_id: {
    type: String,
  },
  gender: {
    type: String,
  },

  appoinment_date: {
    type: String,
  },
  department: {
    type: String,
  },
  designation: {
    type: String,
  },
  dob: {
    type: String,
  },
});

exports.employeeModel = mongoose.model("employee", employeeSchema);
