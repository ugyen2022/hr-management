const mongoose = require("mongoose")
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose. Schema({
    name: {type: String, 
    required:[ true,'Please enter your name!'],
    },
    userid: {
        type: String,
        required: [true, 'Please provide your userid'],
        unique: true, 
    },
    email: {
        type: String,
        required: [true, 'Please provide your email'],
        unique: true, 
        lowercase: true, 
        validate: [validator.isEmail ,'Please provide a valid email'],
    },
    photo: {
        type: String,
        default: 'default.jpg',
    },
    role: {
        type: String, 
        enum: ['admin','employee'],
        default: 'employee'
    }, 
    password:{
        type: String, 
        //required: [true,'Please provide a password!'],
        minlength: 8,
        //password wont be included when we get the users select: false,
    },  
 
    gender:{
        type:String,
        enum:['Male','Female'],
        required: true,
    },
    
    appoinmentdate: {
        type: String, 
        //reuired:[true, 'Please provide your year'],
        // default: 'hos'
    }, 
    department: {
        type: String, 
        //reuired:[true, 'Please provide your course'],
        // default: 'hos'
    }, 
    designation: {
        type: String, 
        //reuired:[true, 'Please provide your year'],
        // default: 'hos'
    }, 
    dob: {
        type: Date, 
        //reuired:[true, 'Please provide your year'],
        // default: 'hos'
    },

    token:{
        type:String,
        default:''
    }, 
    active: {
        type: Boolean, 
        default: true, 
        select: false,
    }
    
})

const User = mongoose.model('User', userSchema)
module.exports = User


