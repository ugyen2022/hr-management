const User = require("../models/userModel");

const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");
const randormstring = require("randomstring");
const path = require("path");
const config = require("../config/config");
const { ifError } = require("assert");
const mongoose = require("mongoose");
const { employeeModel } = require("../models/employeeModel");

const securePassword = async (password) => {
  try {
    const passwordHash = await bcrypt.hash(password, 10);
    return passwordHash;
  } catch (error) {
    console.log(error.message);
  }
};
const loadRegister = async (req, res) => {
  try {
    res.render("admin/register");
  } catch (error) {
    console.log(error.message);
  }
};

const registerAdmin = async (req, res) => {
  try {
    const spassword = await securePassword(req.body.password);
    const admin = new User({
      name: req.body.name,
      userid: req.body.userid,
      email: req.body.email,
      role: req.body.role,
      password: spassword,
      photo: req.file.filename,
    });
    const adminData = await admin.save();

    if (adminData) {
      //verify
      // sendVerifyMail(req.body.name, req.body.email, adminData._id);
      res.render("admin/register", {
        message: "Your registration has been successful!!",
      });
    } else {
      res.render("admin/register", {
        message: "Your registration has been failed ",
      });
    }
  } catch (error) {
    console.log(error.message);
  }
};

const loadDashboard = async (req, res) => {

  try {
    const userData0 = await User.findById({ _id: req.session.userid });
    const users = await employeeModel.find({});

    res.render("admin/adminDashboard", {
      admin: userData0,
      users: users,
    });
  } catch (error) {
    console.log(error.message);
  }
};




const postGender = async (req, res) => {

  try {
    const userData0 = await User.findById({ _id: req.session.userid });
    const users = await employeeModel.find({gender: req.body.gender});

    res.render("admin/adminDashboard", {
      admin: userData0,
      users: users,
    });
  } catch (error) {
    console.log(error.message);
  }
};


const loadIndividualRegisteration = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    res.render("admin/addIndividual", { admin: userData });
  } catch (error) {
    console.log(error.message);
  }
};

const IndividualRegisterationEmployee = async (req, res) => {
  try {
    const name = req.body.name;
    const userid = req.body.sid;
    const appointment_date = req.body.appointment_date;
    const dob = req.body.dob;
    const designation = req.body.designation;
    const gender = req.body.gender;
    const course = req.body.course;

    const employee = new employeeModel({
      name: name,
      employee_id: userid,
      appoinment_date: appointment_date,
      dob: dob,
      designation: designation,
      gender: gender,
      department: course,
    });
    await employee.save();
    console.log("Employee added!");
    const userData = await User.findById({ _id: req.session.userid });
    res.render("admin/addIndividual", { admin: userData, message: "Employee Added" });
  } catch (error) {
    console.log(error.message);
  }
};

const loadEditEmployee = async (req, res) => {
  try {
    const userData1 = await User.findById({ _id: req.session.userid });
    const userData = await employeeModel.findById({ _id: req.params.id });
    if (userData) {
      res.render("admin/editEmployee", { admin: userData1, users: userData });
    } else {
      res.redirect("/ManageEmplpyee");
    }
    res.redirect("admin/editEmployee");
  } catch (error) {
    console.log(error.message);
  }
};
const updateEmployee = async (req, res) => {
  try {
    const userData1 = await User.findById({ _id: req.session.userid });
    await employeeModel.findByIdAndUpdate(
      { _id: req.params.id },
      {
        $set: {
          name: req.body.name,
          employee_id: req.body.sid,
          gender: req.body.gender,
          dob: req.body.dob,
          appoinment_date: req.body.appointment_date,
          department: req.body.department,
          designation: req.body.designation,
        },
      }
    );
    const userData2 = await employeeModel.findById({ _id: req.params.id });

    res.render("admin/editEmployee", {
      admin: userData1,
      users: userData2,
      message: "successfully updated",
    });
  } catch (error) {
    // console.log(error.message);
    const id = req.body.id;
    const userData1 = await User.findById({ _id: req.session.userid });
    const userData2 = await User.findById({ _id: id });
    if (error.code === 11000) {
      res.render("admin/editEmployee", {
        admin: userData1,
        users: userData2,
        message: "email or userid is already registered",
      });
    } else {
      // Handle other errors
      res.render("admin/editEmployee", {
        admin: userData1,
        users: userData2,
        message: "An error occurred",
      });
    }
  }
};

const loademployeeprofile = async (req, res) => {
  try {
    const id = req.query.id;
    const userData = await User.findById({ _id: req.session.userid });
    const userData1 = await User.findById({ _id: id });

    // res.render('admin/studentProfile',{admin:userData,std:userData1,cca:ccaRequests});
    res.render("admin/employeeProfile", {
      admin: userData,
      std: userData1,
    });
  } catch (error) {
    console.log(error.message);
  }
};

const loadsetting = async (req, res) => {
  try {
    const userData = await User.findById({ _id: req.session.userid });
    res.render("admin/setting", { admin: userData });
  } catch (error) {
    console.log(error.message);
  }
};

//delete user
const deleteEmployeeLoad = async (req, res) => {
  try {
    const adminData = await User.findById({ _id: req.session.userid });
    const users = await employeeModel.find({});
    const id = req.body.delete_employee;
    await employeeModel.findByIdAndDelete(id);

    res.render("admin/adminDashboard", {
      admin: adminData,
      users: users,
      message: "Successfully Deleted",
    });
  } catch (error) {
    console.log(error.message);
  }
};

//update user profile
const updateProfile = async (req, res) => {
  const userId = req.session.userid;
  const userData = await User.findById(userId);

  try {
    const photo = req.file.filename;

    if (photo) {
      userData.photo = photo;
      await userData.save();
    }
    const updatedUser = await User.findByIdAndUpdate(userId, {
      new: true,
      runValidators: true,
    });

    res.render("admin/setting", {
      admin: updatedUser,
      message: "Successfully updated",
    });
  } catch (error) {
    return res.render("admin/setting", {
      admin: userData,
      message: "Something went wrong",
    });
  }
};

//update user profile
const updatePassword = async (req, res) => {
  const userId = req.session.userid;
  const userData = await User.findById(userId);

  try {
    const oldPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;
    const confirmPassword = req.body.confirmPassword;

    if (oldPassword && newPassword && confirmPassword) {
      const user = await User.findById(userId).select("+password");

      if (await bcrypt.compare(oldPassword, user.password)) {
        if (newPassword === confirmPassword) {
          const hashedPassword = await securePassword(newPassword);
          user.password = hashedPassword;
          await user.save();

          const updatedUser = await User.findByIdAndUpdate(userId, {
            new: true,
            runValidators: true,
          });

          return res.render("admin/setting", {
            admin: updatedUser,
            message: "Password successfully updated",
          });
        } else {
          return res.render("admin/setting", {
            admin: userData,
            message: "New password and confirm password do not match",
          });
        }
      } else {
        return res.render("admin/setting", {
          admin: userData,
          message: "Old password is incorrect",
        });
      }
    } else {
      return res.render("admin/setting", {
        admin: userData,
        message:
          "Please provide old password, new password, and confirm password",
      });
    }
  } catch (error) {
    return res.render("admin/setting", {
      admin: userData,
      message: "Something went wrong",
    });
  }
};
const loadViewMoreEvent = async (req, res) => {
  try {
    const id = req.query.id;
    const userData = await User.findById({ _id: req.session.userid });
    const eventData = await Event.findById({ _id: id });
    // res.local.data = { id: req.query.id}

    res.render("admin/eventDetails", { admin: userData, event: eventData });
  } catch (error) {
    console.log(error.message);
  }
};
module.exports = {
  loadRegister,
  postGender,
  registerAdmin,
  loadDashboard,
  loadIndividualRegisteration,
  IndividualRegisterationEmployee,
  loadEditEmployee,
  updateEmployee,
  loadsetting,
  loademployeeprofile,
  deleteEmployeeLoad,
  updateProfile,
  updatePassword,
  loadViewMoreEvent,
};
