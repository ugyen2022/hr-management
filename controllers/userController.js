const User = require("../models/userModel");
const bcrypt = require("bcrypt");
const randormstring = require("randomstring");
const AppError = require("./../utils/appError");

const config = require("../config/config");

const securePassword = async (password) => {
  try {
    const passwordHash = await bcrypt.hash(password, 10);
    return passwordHash;
  } catch (error) {
    console.log(error.message);
  }
};

const loadRegister = async (req, res) => {
  try {
    res.render("admin/register");
  } catch (error) {
    console.log(error.message);
  }
};

//login user method
const loginLoad = async (req, res) => {
  try {
    res.render("admin/login");
  } catch (error) {
    console.log(error.message);
  }
};
// user logout
const logout = async (req, res) => {
  try {
    req.session.destroy();
    res.redirect("/");
  } catch (error) {
    console.log(error.message);
  }
};



//load forget passward page
const resetPasswordLoad = async (req, res) => {
  try {
    res.render("admin/reset-password");
  } catch (error) {}
};


//reset password post
const resetPassword = async (req, res) => {
  // console.log("THis is me from reset passwrod")
  try {
    const tokenData = await User.findOne({ token: OTP });
    // console.log(tokenData)
    if (tokenData) {
      const password = req.body.password;
      const cpassword = req.body.cpassword;
      if (password === cpassword) {
        // console.log("This is the new Password "+password);
        const secure_password = await securePassword(cpassword);

        const updatedData = await User.findByIdAndUpdate(
          { _id: tokenData._id },
          { $set: { password: secure_password, token: "" } },
          { new: true }
        );

        res.redirect("/");
      } else {
        res.redirect("/reset-Password");
      }
    }
  } catch (error) {
    console.log(error.message);
  }
};

//verify logi
const verifyLogin = async (req, res) => {
  try {
    const userid = req.body.userid;
    const password = req.body.password;

    const userData = await User.findOne({ userid: userid });

    if (userData) {
      const passwordMatch = await bcrypt.compare(password, userData.password);

      if (passwordMatch) {
        if (userData.role === "employee") {
          req.session.userid = userData._id;
          req.session.role = userData.role;
          return res.redirect("/employeeDashboard");
        } else if (userData.role === "admin") {
          req.session.userid = userData._id;
          req.session.role = userData.role;
          return res.redirect("/adminDashboard");
        } else {
          res.render("admin/login", {
            message: "User_id or Password is incorrect!",
          });
        }
      } else {
        res.render("admin/login", {
          message: "User_id or Password is incorrect!",
        });
      }
    } else {
      res.render("admin/login", {
        message: "User_id or Password is incorrect!",
      });
    }
  } catch (error) {
    console.log(error.message);
  }
};
const restrictTo = (...roles) => {
  return (req, res, next) => {
    if (!req.session.userid || !roles.includes(req.session.role)) {
      return next(
        new AppError("You do not have permission to perform this action", 403)
      );
    }

    next();
  };
};

module.exports = {
  loadRegister,
  loginLoad,
  verifyLogin,
  
  resetPasswordLoad,
  resetPassword,
  
  logout,
  restrictTo,
};
